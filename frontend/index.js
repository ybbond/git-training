const axios = require('axios');
const express = require('express');
const app = express();

app.get('/ping', (req, res) => {
  axios.get('http://api:3000/posts').then((response) => res.send(response));
  // res.send('startttt');
});

app.listen('3000', () => {
  console.log('App run on port 3000');
});
